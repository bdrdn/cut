package Logic;

import Enums.PaymentType;
import Logic.AppInner.*;
import Logic.Universal.*;
import Pages.unversalPages.OfertaPage;
import Enums.Documents;
import inter.MobileLogic;
import inter.MobilePaymentLogic;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;

import static reps.DataBase.DataBase.killOldCharge;

public class SameAppLogic implements MobilePaymentLogic, MobileLogic {

    private static final Logger logger = LoggerFactory.getLogger(SameAppLogic.class);

    private AppiumDriver<MobileElement> driver;

    private AssertAfterPayment aap;
    private Payment Payment;
    private AppSearch search;
    private AssertBeforePayment abp;
    private SaveDocument settings;
    private Banner banner;

    private Documents[] documents;
    private boolean receipt;
    private String paymentId;
    private String totalAmount;
    private boolean sale;

    public SameAppLogic(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        this.abp = new AssertBeforePayment(driver);
        this.search = new AppSearch(driver);
        this.Payment = new Payment(driver);
        this.aap = new AssertAfterPayment(driver);
        this.settings = new SaveDocument(driver);
        this.banner = new Banner(driver);
    }

    @SuppressWarnings("unchecked")
    @Override
    public SameAppLogic asseptOferta() {
        new OfertaPage(driver).asseptOferta();
        return this;
    }

    @Override
    public MobilePaymentLogic search(Documents... docs) {
        this.documents = docs;
        logger.debug("docs for search");
        for (Documents doc : docs) {
            String docType = doc.getType();
            logger.debug(docType);
            switch (docType) {
                case "BUR":
                    search.setI(doc);
                    break;
                case "BURBUR":
                    search.setS(doc);
                    break;
                default:
                    throw new NoSuchElementException("Not valid docType. Must be BUR/BURBUR");
            }
        }
        search.clickSearch();
        return this;
    }

    @Override
    public MobilePaymentLogic validateData() {
        Documents doc = documents[0];
        logger.debug("get doc for search");
        abp.pickFirstPayment();
        this.totalAmount = abp.getTotalAmount();
        abp.clickToPay();
        logger.debug("Going to PaymentPage with doc {}", doc.getsValue());
        return this;
    }

    @Override
    public MobilePaymentLogic pay(boolean receipt, PaymentType Type) {
        this.receipt = receipt;
        logger.debug("Pay type {}",Type);
        if (receipt) Payment.setReceipt();
        this.totalAmount = Payment.getTotalValue();
        logger.debug("TotalAmount {}", totalAmount);
        switch (Type){
            case NORMAL: Payment.normalPay(); break;
            case CANCEL: Payment.CancelPay();break;
            case NOT3DS: Payment.not3DS(); break;
        }
        return this;
    }

    @Override
    public MobilePaymentLogic assertPayment() {
        aap.dataBaseStatus(receipt).validTotalAmount(totalAmount, receipt, sale).assertAppType("appType");
        this.paymentId = aap.getPaymentId();
        return this;
    }

    @Override
    public String getPaymentId() {
        return paymentId;
    }

    public void checkPaymentHistory(){
        aap
                .goToFinesPage()
                .getHistoryPaymentsPage()
                .historyContain(documents[0].getsValue());
    }

    @Override
    public MobileLogic saveCharges(Documents... docs) {
        this.documents = docs;
        for (Documents doc : docs) {
            killOldCharge(doc);
            settings
                    .clickSetings()
                    .fillSearchDoc(doc);
        }
        settings.goToMainPage();
        return this;
    }

//    @Override
    public MobileLogic checkSaveCharges() {
        Assert.fail("add selector pls");
//        for (Documents doc : documents);
//            abp.paymentListContain(doc.getsValue());
        return this;
    }

    @Override
    public MobileLogic bannerVisaCheck(){
        this.search(Documents.APP_DOC_1);
        banner.clickPaymentBanner().clickMenuBanner().noMorePaymentBanner();
        return this;
    }

    @Override
    public MobileLogic checkRating(){
        new RatingLogic(driver).getRatingWindow();
        return this;
    }
}
