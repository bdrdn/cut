package Logic.Universal;

import Pages.SameApp.MainPageapp;
import Pages.unversalPages.HistoryPaymentsPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HistoryLogic {
    private static final Logger logger = LoggerFactory.getLogger(HistoryLogic.class);

    private HistoryPaymentsPage historyPaymentsPage;
    private AppiumDriver<MobileElement> driver;

    public HistoryLogic(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        historyPaymentsPage = new HistoryPaymentsPage(driver);

    }

    public HistoryLogic getHistoryPaymentsPage(){
        new MainPageapp(driver).clickByMenuButton().historyClick();
        return this;
    }

    public HistoryLogic historyContain(String u){
        Assert.assertTrue("List doesn`t contain such u: "+ u, historyPaymentsPage.getTextListFromPayments().contains(u));
        return this;
    }
}
