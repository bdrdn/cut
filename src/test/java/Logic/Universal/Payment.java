package Logic.Universal;

import Pages.unversalPages.CryptoCardPage;
import Pages.unversalPages.PayPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static reps.Params.EMAIL;

public class Payment {
    private static final Logger logger = LoggerFactory.getLogger(Payment.class);
    private AppiumDriver<MobileElement> driver;
    private PayPage payPage;
    private CryptoCardPage ccp;


    public Payment(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        this.payPage = new PayPage(driver);
    }

    public String getTotalValue(){
        return payPage.getSumVal();
    }

    public Payment setReceipt(){
        logger.debug("try to set Receipt");
        pickReceipt(EMAIL);
//        Assert.assertTrue("",);
        return this;
    }

    public Payment normalPay() {
        logger.debug("start normalPay");
        setPhoneAndName("9999999999");
        fillCard();
        return this;
    }

    public Payment CancelPay(){
        logger.debug("start CancelPay");
        setPhoneAndName("9999999991");
        fillCard();
        return this;
    }

    public Payment not3DS(){
        logger.debug("start not3DS");
        setPhoneAndName("9999999992");
        fillCard();
        return this;
    }

    private void fillCard(){
        ccp.fillCardPayPage().payButtonClick();
        logger.debug("fillCard complete.");
    }

    private void setPhoneAndName(String phone){
        payPage.fillFio("Фау Мобильная");
        payPage.fillPhone(phone);
        this.ccp = payPage.payButtonClick();
    }

    private void pickReceipt(String email) {
        payPage.kvitClickAndFill(email);
    }
}
