package Logic.Universal;

import Pages.unversalPages.PayFinePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;

public class AssertBeforePayment {

    private PayFinePage pfp;

    public AssertBeforePayment (AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        this.pfp = new PayFinePage(driver);
    }

    private AppiumDriver<MobileElement> driver;

    public AssertBeforePayment saleValue(){
        Assert.assertTrue("doesn't contain sale block",pfp.isDiscountPresent());
        return this;
    }

    public AssertBeforePayment pickS(String sValue){
        pfp.choicePay(sValue);
        return this;
    }

    public AssertBeforePayment pickFirstPayment(){
        pfp.choicePay();
        return this;
    }

    public AssertBeforePayment paymentListContain(String Text){
        pfp.paymentListContain(Text);
        return this;
    }

    public String getTotalAmount(){
        return pfp.getTotalSumValue();
    }

    public void clickToPay(){
        pfp.payButtonClick();
    }

}
