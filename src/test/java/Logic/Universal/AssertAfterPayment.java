package Logic.Universal;

import Pages.unversalPages.ResultPaymentPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import Enums.DB_Param;
import reps.DataBase.DataBase;

import static reps.Utils.parseInt;

public class AssertAfterPayment {

    private DataBase db = new DataBase(DB_Param.ANOTHERDB.SHARD1);
    private static final Logger logger = LoggerFactory.getLogger(AssertAfterPayment.class);

    private String paymentId;
    private boolean receipt;
    private String deviceId;
    private String amount, totalAmount, totalAmountHeader, amountDB, totalAmountDB, comission, receiptValue;

    public AssertAfterPayment(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        this.rpp = new ResultPaymentPage(driver);
    }

    private AppiumDriver<MobileElement> driver;
    private ResultPaymentPage rpp;

    public AssertAfterPayment validTotalAmount(String totalAmount, boolean receipt, boolean sale){
        logger.debug("try to validate amounts");
        this.receipt = receipt;
        int totalA = parseInt(this.totalAmount)-parseInt(comission);
        int fullAmount = parseInt(this.amount);
        if(this.receipt) totalA -=3500;
        Assert.assertEquals("Wrong payment sum",totalA,parseInt(totalAmount));
        Assert.assertEquals("total amount not equal headerAmount",this.totalAmount,totalAmountHeader);
        if (sale) fullAmount = fullAmount/2;
        Assert.assertEquals("wrong pay sum",totalA,fullAmount);
        logger.debug("Success");
        return this;
    }

    public AssertAfterPayment dataBaseStatus(boolean receipt){
        this.receipt = receipt;
        this.paymentId = rpp.getPcId();
        logger.debug("Try to get payment {} status", paymentId);
        String status = db.getPaymentStatus(paymentId);
        Assert.assertEquals("Wrong status on payment "+paymentId,"success status",status);
        logger.debug("Success status for payment {}",paymentId);
        getAmounts();
        assertReceipt();
        this.deviceId = db.selectFromPayments(paymentId,"device_id");
        return this;
    }

    public AssertAfterPayment assertAppType(String app){
        logger.debug("Try to validate appType for device {} must be ", deviceId, app);
        String apptype = db.getAppType(deviceId);
        Assert.assertEquals("Wrong app_type", app, apptype);
        logger.debug("Success");
        return this;
    }

    public HistoryLogic goToFinesPage(){
        rpp.clickByGoToFinesListButton();
        return  new HistoryLogic(driver);
    }

    public String getPaymentId(){
        return this.paymentId;
    }

    private void getAmounts(){
        this.amountDB = db.selectFromPayments(paymentId,"amount");
        this.totalAmountDB = db.selectFromPayments(paymentId,"total_amount");
        this.comission = rpp.getCommissionValue();
        this.totalAmountHeader = rpp.getTotalAmountHeaderValue();
        this.totalAmount = rpp.getTotalAmount();
        this.amount = rpp.getFullPaymentValue();
        this.receiptValue = rpp.getKvitValue();

    }

    private void assertReceipt(){
        if(receipt) {
            logger.debug("Checking receipt value");
            Assert.assertTrue("in payment with receipt"+amountDB +"!<"+ totalAmountDB+"in payment "+paymentId,
                    parseInt(amountDB)<
                            parseInt(totalAmountDB));
            Assert.assertEquals("amount " + amountDB + "totalAmount " + totalAmountDB + "in payment " + paymentId,
                    parseInt(amountDB) + parseInt(receiptValue)+parseInt(comission), parseInt(totalAmountDB));
        } else Assert.assertEquals(amountDB,totalAmountDB);
    }
}
