package Logic.Universal;

import Pages.unversalPages.PayFinePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Banner {

    private final static Logger logger = LoggerFactory.getLogger(Banner.class);

    private AppiumDriver<MobileElement> driver;
    private PayFinePage pfp;

    public Banner(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        this.pfp = new PayFinePage(driver);
    }

    public Banner clickPaymentBanner(){
        pfp.clickBannerButton().validateText("Цени время ").clickReturnButton();
        return this;
    }

    public Banner clickMenuBanner(){
        pfp.clickMenuButton().visaBannerClick().validateText("Цени время ").clickReturnButton();
        return this;
    }

    public Banner noMorePaymentBanner(){
        try{
            pfp.clickBannerButton();
            Assert.fail("payments page still contains banner");
        } catch (Exception e){
            e.printStackTrace();
        }
        return this;
    }

}
