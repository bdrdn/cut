package Logic.Universal;

import Pages.unversalPages.AbstractMenu;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RatingLogic {

    private static final Logger logger = LoggerFactory.getLogger(RatingLogic.class);
    private AppiumDriver<MobileElement> driver;

    public RatingLogic(AppiumDriver<MobileElement> driver){
        this.driver = driver;
    }

    public RatingLogic getRatingWindow(){
        new AbstractMenu(driver).raiting1Click().clickBySendResponseButton("ToraToraTora");
        Assert.fail("Уточнить про возможность замены почты на внешнюю");
        return this;
    }
}
