package Logic.AppInner;

import Enums.Documents;
import Pages.SameApp.MainPageapp;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static reps.DataBase.DataBase.killOldCharge;

public class AppSearch {
    private static final Logger logger = LoggerFactory.getLogger(AppSearch.class);

    private MainPageapp mainPage;

    public AppSearch(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        mainPage = new MainPageapp(driver);
    }

    private AppiumDriver<MobileElement> driver;

    public AppSearch setI(Documents doc) {
        killOldCharge(doc);
        mainPage.setnValue(doc.getdValue());
        return this;
    }

    public AppSearch setS(Documents doc) {
        killOldCharge(doc);
        mainPage
                .clickByGoToUSearchButton()
                .setUValue(doc.getsValue());
        return this;
    }

    public void clickSearch(){
        mainPage.clickBySearchButton();
    }
}
