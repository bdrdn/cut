package Logic.AppInner;

import Pages.SameApp.MainPageapp;
import Pages.SameApp.SettingsPageApp;
import Enums.Documents;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static reps.Utils.sleep_for;

public class SaveDocument {
    private static final Logger logger = LoggerFactory.getLogger(SaveDocument.class);

    private MainPageapp mainPage;
    private SettingsPageApp settingsPage;

    public SaveDocument(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        mainPage = new MainPageapp(driver);
    }

    private AppiumDriver<MobileElement> driver;

    public SaveDocument clickSetings() {
        this.settingsPage = mainPage
                                .clickByMenuButton()
                                .settingsClick();
        return this;
    }

    public void fillSearchDoc(Documents doc){
        settingsPage.setIValue(doc.getdValue());
    }

    public void switchPushCheckBox(){
        settingsPage.switchPushCheckBox();
    }

    private void saveSettings(){
        settingsPage.clickSaveButton();
        sleep_for(5500);
    }

    public void goToMainPage(){
        saveSettings();
        mainPage.clickByMenuButton().clickSearchN();
    }
}
