package reps;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppUtils {

    private static final Logger logger = LoggerFactory.getLogger(AppUtils.class);

    private static Properties prop = new Properties();

    private void loadConfigProp(String propertyFileName) throws IOException {
//Sorry but nope
    }


    private DesiredCapabilities setDesiredCapabilities(String appType) {

        DesiredCapabilities cap = new DesiredCapabilities();

        return cap;
    }

    public AppiumDriver<MobileElement> setup(String appType) {
        String propertyFileName = null;
        if(appType.equalsIgnoreCase("someapp")){
            propertyFileName = "application.properties";
        }
        try {
            loadConfigProp(propertyFileName);
        } catch (IOException e) {
            logger.error("Can't read property file {}", propertyFileName, e);
        }

//        for no red lines
//        AppiumDriver<MobileElement> driver ;
//        driver = new AndroidDriver<>("serverUrl from Capabilities", setDesiredCapabilities(appType));
//
        logger.debug("Got {} driver", "PLATFORM_NAME_VALUE");
        return null;
    }
}