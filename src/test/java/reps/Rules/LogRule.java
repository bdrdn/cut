package reps.Rules;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;


public class LogRule extends TestWatcher {
    private static final Logger logger = LoggerFactory.getLogger(LogRule.class);
    private WebDriver driver;

    public static String failedTestScreen = "./src/scrnSelen/FailedTestScreen/";
    public static String TestScreenDir = "./src/scrnSelen/testScreen/";

    public LogRule(WebDriver driver) {
        this.driver = driver;
        String logToken = getLogToken.getLogToken(Thread.currentThread());
        MDC.put("logToken", logToken);
    }

    public LogRule() {
        this.driver = null;
        String logToken = getLogToken.getLogToken(Thread.currentThread());
        MDC.put("logToken", logToken);
    }

    @Override
    protected void starting(Description description) {
        logger.info("Запуск теста: {}", description.getMethodName());
    }

    @Override
    protected void failed(Throwable e, Description description) {
        if (driver != null) {
            if (e instanceof AssertionError) {
                logFinishStatus(description.getMethodName(), "неудачно; Скриншот \n"
                        + getPath(description));
            } else {
                logFinishStatus(description.getMethodName(), "с ошибкой; Скриншот \n"
                        + getPath(description));
                logger.error("Exception: {}", e.getLocalizedMessage());
            }
            logger.debug(description.getDisplayName(), e);
        }
    }

    private String getPath(Description description) {
        return new File(failedTestScreen.replace("./", "")).getAbsolutePath() + "\\" + new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime()) + "_testrun\\" + description.getMethodName() + ".png";
    }

    @Override
    protected void succeeded(Description description) {
        if (driver != null)
            logFinishStatus(description.getMethodName(), "success, Screenshot " + stepScreenshotInner(description));
    }

    @Override
    protected void finished(Description description) {
        if (driver != null) {
            LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
            List<String> e = new ArrayList<>();
            for (LogEntry entry : logEntries.getAll()) {
                if (entry.getLevel().equals(Level.SEVERE)) {
                    e.add(entry.getMessage());
                }
            }
            List<String> list = e.stream().distinct().collect(Collectors.toList());
            for (String entry : list)
                logger.debug(entry);
        }
    }

    public Logger log() {
        return logger;
    }

    private void logFinishStatus(String methodName, String status) {
        logger.info("Завершён тест : {}", methodName);
        logger.info("Результат : {}", status);
    }

    private String stepScreenshotInner(Description description) {
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime());
        Screenshot screenshot = new AShot().takeScreenshot(driver);
        new File(TestScreenDir + timeStamp + "_testrun\\").mkdirs();
        String path = TestScreenDir + timeStamp + "_testrun\\" + description.getMethodName() + Long.toString(new Date().getTime()) + ".png";
        File destFile = new File(path);
        try {
            ImageIO.write(screenshot.getImage(), "PNG", destFile);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        return new File(path.replace("./", "")).getAbsolutePath();
    }
}

