package reps.Rules;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reps.a.CaseID;

import java.lang.reflect.Method;

import static reps.Params.run_id;
import static reps.TestRail.rail.postTestResult;
import static reps.Utils.parseInt;


public class ResultCollector extends TestWatcher {

    private final static Logger logger = LoggerFactory.getLogger(ResultCollector.class);
    private AppiumDriver<MobileElement> driver;

    public ResultCollector(AppiumDriver<MobileElement> driver){
        this.driver = driver;
    }

    public ResultCollector(){
        this.driver=null;
    }

    private String comment = "";

    public void addComment(String comment) {
        this.comment = comment;
    }

    @Override
    protected void succeeded(Description d){
        Asserter(comment,d,1);
    }

    @Override
    protected void starting(Description d) {
    }

    @Override
    protected void failed(Throwable e, Description description) {
        int status;
        if(e.toString().contains("AssertionError")) status = 5;
        else status = 4;
        Asserter(e.getLocalizedMessage(),description,status);
    }

    @Override
    protected void finished(Description description) {
        if(this.driver!=null) driver.quit();
    }

    private void Asserter(String ex, Description description,int status){
            String testName = description.getMethodName();
        if(!run_id.equals("")) {
            try {
                Method current = description.getTestClass().getMethod(testName);
                CaseID anatation = current.getAnnotation(CaseID.class);
                String[] caseIds = anatation.toString().split(",");
                for (String caseId1 : caseIds) {
                    String caseId = Integer.toString(parseInt(caseId1));
                    postTestResult(run_id, caseId, status, testName, ex);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            logger.debug("No testrail report for test {} , end with msg {}", testName, ex);
        }
    }
}
