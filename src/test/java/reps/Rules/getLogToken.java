package reps.Rules;


import reps.HashWorker;

public class getLogToken {
    public static String getLogToken(Thread threadName) {
        String logToken = System.currentTimeMillis() + threadName.getName();
        return HashWorker.hashString(logToken, "md5");
    }
}