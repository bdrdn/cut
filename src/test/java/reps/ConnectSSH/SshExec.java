package reps.ConnectSSH;

import Enums.SshServerProps;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class SshExec {
    private static final Logger logger = LoggerFactory.getLogger(SshExec.class);
    private String host, user, password;
    private int port;

    public SshExec(SshServerProps PROJECT) {
        this.host = PROJECT.getProps().get("host");
        this.user = PROJECT.getProps().get("user");
        this.password = PROJECT.getProps().get("password");
        this.port = Integer.parseInt(PROJECT.getProps().get("port"));
    }

    public List<String> customComand(String command) {
        List<String> lines = new ArrayList<>();
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, port);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword(password);
            session.connect();
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);
            InputStream input = channel.getInputStream();
            channel.connect();
            logger.info("Channel Connected to machine " + host + " server with command: " + command);
            try {
                InputStreamReader inputReader = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    logger.info(line);
                    lines.add(line);
                }
                bufferedReader.close();
                inputReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            channel.disconnect();
            session.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lines;
    }
}
