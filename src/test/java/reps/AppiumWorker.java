package reps;

import java.io.IOException;
import java.net.ServerSocket;

public class AppiumWorker {

    public void startServer() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
            do{
            Thread.sleep(15000);
            } while (checkIfServerIsRunnning(4723));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIfServerIsRunnning(int port) {

        boolean isServerRunning = false;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.close();
        } catch (IOException e) {
            isServerRunning = true;
        } finally {
            serverSocket = null;
        }
        return isServerRunning;
    }

    public void stopServer() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("taskkill /F /IM node.exe");
            runtime.exec("taskkill /F /IM cmd.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        AppiumWorker appiumServer = new AppiumWorker();
        appiumServer.startServer();
        appiumServer.stopServer();
    }
}
