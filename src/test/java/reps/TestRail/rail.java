package reps.TestRail;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static reps.Params.*;


public class rail {

    private static final Logger logger = LoggerFactory.getLogger(rail.class);

    public rail(){
        pickParentRunId();
    }

    public static ArrayList<String> getFailedTestsRun(String runid) {
        JSONArray c;
        ArrayList<String> idFail = new ArrayList<>();
        String strings;
        APIClient client = new APIClient("");
        client.setUser("");
        client.setPassword("");
        try {
            if (System.getProperty("full_run", null) != null) {
                c = (JSONArray) client.sendGet("get_tests/".concat(runid));
            } else {
                c = (JSONArray) client.sendGet("get_tests/" + runid + "&status_id=4,5,3");
            }
            //получение имен завалившихся тестов from test run
            for (Object aC : c) {
                JSONObject slide = (JSONObject) aC;
                strings = slide.get("case_id").toString();
                idFail.add(strings);
            }
        } catch (Exception ignored) {
        }
        return idFail;

    }

    public static void postTestResult(String runid, String caseId, int status_id, String comment, String ex){
        String strings = comment + "\n" + "LogToken: " + MDC.get("logToken") + "\n" + ex;
        APIClient client = new APIClient("");
        client.setUser("");
        client.setPassword("");
        Map data = new HashMap();
        data.put("status_id", status_id);
        data.put("comment", strings);
        try {
            client.sendPost("add_result_for_case/" + runid + "/" + caseId, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String parseCaseIdToString(String runid) {
        return String.join(", ", getFailedTestsRun(runid));
    }

    public String clone_run(String testrail_parent_run_id) {
        if (!(testrail_parent_run_id == null)) {
            ArrayList<String> idFail = new ArrayList<>();
            APIClient client = new APIClient("https://rscom.testrail.net");
            client.setUser("");
            client.setPassword("");
            try {
                JSONArray c = (JSONArray) client.sendGet("get_tests/".concat(testrail_parent_run_id));
                for (Object aC : c) {
                    JSONObject slide = (JSONObject) aC;
                    String strings = slide.get("case_id").toString();
                    idFail.add(strings);
                }
                Map data = new HashMap();
                data.put("include_all", false);
                data.put("case_ids", idFail);
                data.put("name", testrail_clone_run_name + " " + productVersion());
                JSONObject m = (JSONObject) client.sendPost("add_run/".concat(pickProjectId()), data);
                return m.get("id").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger.info("case ids {}",idFail);
        }
        return System.getProperty("run_id", Integer.toString(0));
    }

    private String productVersion() {
        String dateTime = new SimpleDateFormat("yyyy-mm-dd hh:mm").format(Calendar.getInstance().getTime());
        if(testrail_parent_run_id != null){
            switch (testrail_parent_run_id){
                case "1111":
                    return " ".concat(dateTime);
                case "2222":
                    return " ".concat(dateTime);
                case "3333":
                    return " ".concat(dateTime);
            }
        }
        return dateTime;
    }

    private static void pickParentRunId(){
        if(testrail_parent_run_id == null && parent_project != null){
            parent_project = parent_project.toLowerCase();
            switch (parent_project.toLowerCase()){
                case "app":
                    testrail_parent_run_id = "1111";
                    break;
                case "sameapp":
                    testrail_parent_run_id = "2222";
                    break;
                case "otherapp":
                    testrail_parent_run_id = "3333";
                    break;
            }
        }
    }



    private static String pickProjectId() {
        if (reps.Params.testrail_parent_run_id != null && testrail_project_id == null) {
            return "13";
        }
        return System.getProperty("testrail_project_id");
    }
}