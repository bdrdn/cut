package reps;

public class Utils {
    public static int parseInt(String value) {
        int result;
        String stringValue = value.replaceAll("[^\\d]", "");
        result = Integer.parseInt(stringValue);
        return result;
    }

    public static String redisDelete(String doc, String db) {
        return String.format("redis-cli -n %s KEYS \"*%s*\" | xargs redis-cli -n %s del", db, doc, db);
    }

    public static void sleep_for(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}