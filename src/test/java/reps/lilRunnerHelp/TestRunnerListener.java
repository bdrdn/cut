package reps.lilRunnerHelp;

import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestRunnerListener extends RunListener {
    private static final Logger logger = LoggerFactory.getLogger(TestRunnerListener.class);
    private long startRunTime;

    public TestRunnerListener() {
        startRunTime = System.currentTimeMillis();
    }

    @Override
    public void testRunFinished(Result result) throws Exception {
        summary(result);
    }

    private void summary(Result result) {
        logger.info("Total tests: {}", result.getRunCount());
        logger.info("Failed tests: {}", result.getFailureCount());
        printTimeFormat((System.currentTimeMillis() - startRunTime) / 1000);
    }

    //форматированный вывод времени
    private void printTimeFormat(long timeInSec) {
        long minutes = timeInSec / 60;
        double seconds = timeInSec % 60;
        logger.info(String.format("Timing: %d min %.0f s", minutes, seconds));
    }
}

