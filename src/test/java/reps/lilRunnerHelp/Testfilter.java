package reps.lilRunnerHelp;

import reps.a.CaseID;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;

import java.util.List;

public class Testfilter extends Filter {
    private List<String> anno;

    public Testfilter(List<String> testRunCaseIDs) {
        anno = testRunCaseIDs;
    }


    @Override
    public String describe() {
        return "TestCase Filter";
    }

    @Override
    public boolean shouldRun(Description description) {
        if (description != null) {
            if (description.isSuite()) {
//                чекаем что класс часть тестъсьюта
                return true;
            }
//            а тут уже чекам методы класса на анатацию.
            if (description.getAnnotation(CaseID.class) != null) {
                for (String s : description.getAnnotation(CaseID.class).value()) {
                    if (anno.contains(s)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

