package reps;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashWorker {
    private HashWorker() {
    }

    public static String hashString(String message, String algorithm) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
            return convertByteArrayToHexString(hashedBytes);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException var4) {
            throw new RuntimeException("Не удалось сгенерировать хэш из строки", var4);
        }
    }

    public static String hashStringBase64Encode(String message, String algorithm) {
        return base64Encode(hashString(message, algorithm));
    }

    private static String base64Encode(String s) {
        return Base64.encodeBase64String(s.getBytes());
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();

        for (byte arrayByte : arrayBytes) {
            stringBuffer.append(Integer.toString((arrayByte & 255) + 256, 16).substring(1));
        }

        return stringBuffer.toString();
    }
}