package reps.DataBase;

import Enums.DB_Param;
import Enums.Documents;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reps.ConnectSSH.SshExec;
import Enums.SshServerProps;

import java.sql.*;
import java.util.NoSuchElementException;

import static reps.Params.DB_IP;
import static reps.Params.OTHER_DB_IP;
import static reps.Utils.redisDelete;


public class DataBase {

    private String url;
    private String user = "";
    private String password = "";
    private Connection con;
    private Statement stmt;
    private ResultSet rs;
    private DB_Param.ANOTHERDB test;

    private static final Logger logger = LoggerFactory.getLogger(DataBase.class);

    public DataBase(DB_Param.ANOTHERDB shard){

        this.test = shard;
        this.url = prepUrl(OTHER_DB_IP).concat(shard.getShard()).concat("?characterEncoding=UTF-8");
    }

    public DataBase(DB_Param.DB Shard){
        String shard = Shard.getShard();
        this.url = prepUrl(DB_IP).concat(shard).concat("?characterEncoding=UTF-8");
    }

    public DataBase(String IP, String SHARD) {
        this.url = "jdbc:mysql://".concat(IP).concat("/").concat(SHARD).concat("?characterEncoding=UTF-8");
    }

    public String getAppType(String Id) {
        switch (test) {
            case SHARD1:
                return customSelect("select * from device where id =\"" + Id + "\";", "app_type");
            default:
                throw new NoSuchElementException("Only for mappdb");
        }
    }

    public String selectFromPayments(String Id, String column) {
        switch (test) {
            case SHARD1:
                return customSelect("select * from pay where id =\"" + Id + "\";", column);
            default:
                throw new NoSuchElementException("Only for mappdb");
        }
    }

    public String getPaymentStatus(String ID){
        switch (test){
            case SHARD2:
                return customSelect("select * from pay where id =\""+ID+"\";","status");
            case SHARD1:
                return customSelect("select * from pay whereid = \""+ID+"\";","status");
            default:
                throw new NoSuchElementException("Only for test or mappdb");
        }
    }

    public String customSelect(String query, String column) {
        String r = null;
        logger.debug("select {} column from {}", column, query);
        try {
            initConnect();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                r = rs.getString(column);
                if (r != null) return r;
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            Assert.fail("sqlException");
        } finally {
            try {
                con.close();
                stmt.close();
                rs.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return r;
    }

    public void customUpdate(String query) {
        try {
            initConnect();
            stmt.executeUpdate(query);
        } catch (Exception se) {
            se.printStackTrace();
        } finally {

            try {
                con.close();
                stmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
                Assert.fail(se.getMessage());
            } catch (NullPointerException ignored) {
            }
        }
    }

    public void customDelete(String query) {
        try {
            String myDriver = "org.gjt.mm.mysql.Driver";
            Class.forName(myDriver);
            con = DriverManager.getConnection(url, user, password);
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.execute();
            logger.debug("query {} executed!", query);
        } catch (Exception sqlEX) {
            sqlEX.printStackTrace();
            Assert.fail("cant delete");
        } finally {
            //close connection
            try {
                con.close();
            } catch (SQLException se) {
                se.printStackTrace();
                Assert.fail(se.getMessage());
            }
        }
    }

    public static void killOldCharge(Documents doc) {
        logger.debug("Kill old charges for {} search {}", doc.getType(), doc.getsValue());
        new DataBase(DB_Param.ANOTHERDB.SHARD1).customUpdate("UPDATE pay SET m = 30 WHERE b =\"" + doc.getsValue() + "\" AND status !=3");
        new DataBase(DB_Param.DB.ANOTHERSHARD).customDelete("DELETE FROM paid where c =\"" + doc.getsValue() + "\";");
        new SshExec(SshServerProps.ANDONEMORE).customComand(redisDelete(doc.getdValue(), "2"));
    }

    private String prepUrl(String rawURL){
        return "jdbc:mysql://"+rawURL+"/";
    }

    private void initConnect() throws SQLException {
        con = DriverManager.getConnection(url, user, password);
        stmt = con.createStatement();
    }
}
