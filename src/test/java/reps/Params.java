package reps;


public class Params {
    public static String run_id = System.getProperty("run_id", "");
    static public String parent_project = System.getProperty("project",null);
    public static String testrail_parent_run_id = System.getProperty("testrail_parent_run_id", null);
    public static String testrail_clone_run_name = System.getProperty("testrail_clone_run_name", "Мобильная регрессия");
    public static String testrail_project_id = System.getProperty("testrail_project_id", null);

    public static final String EMAIL = "some@mail.com";

    public static final String DB_IP = "5.188.104.100";
    public static final String OTHER_DB_IP = "5.188.104.91";

}
