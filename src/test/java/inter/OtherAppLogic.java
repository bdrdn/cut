package inter;

import Enums.Documents;

public interface OtherAppLogic extends MobileLogic {

    OtherAppLogic checkPhoto();
    void checkPaymentHistory();
    OtherAppLogic saveCharges(Documents... docs);
    OtherAppLogic checkSaveCharges();

}
