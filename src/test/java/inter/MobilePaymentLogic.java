package inter;

import Enums.PaymentType;
import Enums.Documents;

public interface MobilePaymentLogic {
    <T extends MobileLogic> T asseptOferta();

    MobilePaymentLogic search(Documents... doc);

    MobilePaymentLogic validateData();

    MobilePaymentLogic pay(boolean receipt, PaymentType Type);

    MobilePaymentLogic assertPayment();

    String getPaymentId();
}

