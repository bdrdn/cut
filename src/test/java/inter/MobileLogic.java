package inter;

import Enums.Documents;

public interface MobileLogic {
//    <T> T asseptOferta();

    void checkPaymentHistory();

    MobileLogic saveCharges(Documents... docs);

    MobileLogic checkSaveCharges();

    MobileLogic bannerVisaCheck();

    MobileLogic checkRating();
}