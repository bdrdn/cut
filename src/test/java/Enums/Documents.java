package Enums;

public enum Documents {
    SAME_APP_DOC("value1","value2","SAME_APP_DOC_NAME","true"),
    SAME_APP_DOC2("value1","value2","SAME_APP_DOC2","true"),
    ANOTHER_APP_DOC("value1","value2","ANOTHER_APP_DOC","true"),
    ANOTHER_APP_DOC2("value1","value2","ANOTHER_APP_DOC2","false"),
    APP_DOC_1("value1","value2","APP_DOC_1","false"),
    APP_DOC("value1","value2","APP_DOC","false");

    private String dValue;
    private String sValue;
    private String type;
    private String s;

    Documents(String ... s){
        this.dValue = s[0];
        this.sValue = s[1];
        try {
            this.type = s[2];
            this.s = s[3];
        } catch (ArrayIndexOutOfBoundsException ignored){}
    }

    public String getType(){
        return type;
    }

    public String getdValue(){
        return dValue;
    }

    public String getsValue(){
        return sValue;
    }

    public boolean haveS(){
        return s.equals("true");
    }
}
