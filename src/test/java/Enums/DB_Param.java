package Enums;


public class DB_Param {
    public enum DB {
        SHARD("shardname"), ANOTHERSHARD("shardname");

        private String shard;

        DB(String s) {
            this.shard = s;
        }


        public String getShard() {
            return shard;
        }
    }

    public enum ANOTHERDB {
        SHARD("shardname"), SHARD1("shardname"), SHARD2("shardname"), SHARD3("shardname"),
        SHARD4("shardname"), SHARD5("shardname");

        private String shard;

        ANOTHERDB(String s) {
            this.shard = s;
        }

        public String getShard() {
            return shard;
        }
    }
}
