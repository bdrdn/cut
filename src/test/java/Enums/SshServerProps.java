package Enums;

import java.util.HashMap;
import java.util.Map;

public enum SshServerProps {
    SAMESERVER("ip,password"),
    ANOTHERONE("ip,password"),
    ANDONEMORE("ip,password"),
    OH("ip,password");

    private String user = "";
    private String port = "";
    private Map<String, String> props = new HashMap<>();

    SshServerProps(String s) {
        this.props.put("host",s.split(",")[0]);
        this.props.put("password",s.split(",")[1]);
        this.props.put("port",port);
        this.props.put("user", user);
    }
    public Map<String, String> getProps() {
        return props;
    }
}

