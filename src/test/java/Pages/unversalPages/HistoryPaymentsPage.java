package Pages.unversalPages;

import Pages.AbstractPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static reps.Utils.sleep_for;

public class HistoryPaymentsPage extends AbstractPage {

    public HistoryPaymentsPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public int getPaymentsCount(){
        sleep_for(1000);
        return getPayments().size();


    }

    private List<MobileElement> getPayments(){
        List<MobileElement> payments = getDriver().findElements(By.className("android.widget.Button"));
        payments = payments.subList(1, payments.size());
        return payments;
    }


    public List<String> getTextListFromPayments(){
        List<MobileElement> payments = getPayments();
        List<String> textList = new ArrayList<>();
        for (MobileElement mE: payments) {
            textList.add(mE.getText());
        }
        return textList;
    }
}
