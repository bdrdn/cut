package Pages.unversalPages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import Pages.AbstractPage;

import java.util.List;

import static reps.Utils.sleep_for;

public class PayFinePage extends AbstractPage {
    //private AppiumDriver<MobileElement> driver;

    public PayFinePage(AppiumDriver<MobileElement> driver) {
        super(driver);
        //this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "")
    private MobileElement totalSum;

    @AndroidFindBy(xpath = "")
    private MobileElement foundFineCount;

    @AndroidFindBy(xpath = "")
    private MobileElement discount;

    @AndroidFindBy(xpath = "")
    private MobileElement menuButton;

    @AndroidFindBy(xpath = "")
    private MobileElement bannerButton;

    public AbstractMenu clickMenuButton(){
        waitAndClick(menuButton);
        return new AbstractMenu(getDriver());
    }

    public BannerPage clickBannerButton(){
        waitAndClick(bannerButton);
        return new BannerPage(getDriver());
    }

    public void choicePay(String s) {
        paymentListContain(s).click();
    }

    public void choicePay() {
        paymentListContain().click();
    }

    public MobileElement paymentListContain(String text) {
        MobileElement fine = null;
        sleep_for(200);
        waitListElement("//*[@resource-id = 'submit#goToPay']");
        List<MobileElement> fineItems = getDriver().findElementsByXPath("//*[@resource-id = 'submit#goToPay']");
        for (MobileElement f : fineItems) {
            if (f.getText().contains(text)) fine = f;
        }
        if (fine == null) Assert.fail("paymentList doesn't contain " + text);
        return fine;
    }

    private MobileElement paymentListContain() {
        MobileElement fine;
        sleep_for(200);
        waitListElement("//*[@resource-id = 'submit#goToPay']");
        List<MobileElement> fineItems = getDriver().findElementsByXPath("//*[@resource-id = 'submit#goToPay']");
        fine = fineItems.get(0);
        if (fine == null) Assert.fail("paymentList doesn't contain payments");
        return fine;
    }

    public void payButtonClick() {
        sleep_for(250);
        waitListElement("//*[@resource-id = 'submit#goToPay']");
        List<MobileElement> fineItems = getDriver().findElementsByXPath("//*[@resource-id = 'submit#goToPay']");
        new WebDriverWait(getDriver(), 15)
                .withMessage("need more time for payButton").until(driver -> fineItems.get(0).isDisplayed());
        fineItems.get(0).click();
    }

    private void waitListElement(String xpathToElement) {
        new WebDriverWait(getDriver(), 15)
                .withMessage("need more time for elementsList").until(driver -> {
            List<MobileElement> fineItem = getDriver().findElementsByXPath(xpathToElement);
            if(fineItem.isEmpty()) return false;
            return fineItem.get(0).isDisplayed();
        });
    }

    public String getTotalSumValue(){
        return waitAndGetText(totalSum);
    }

    public String getFoundFineCountValue(){
        return waitAndGetText(foundFineCount);
    }

    public boolean isDiscountPresent(){
        return discount.isDisplayed();
    }

}
