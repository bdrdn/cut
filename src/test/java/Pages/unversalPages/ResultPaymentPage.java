package Pages.unversalPages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import Pages.AbstractPage;

import java.util.concurrent.TimeUnit;

public class ResultPaymentPage extends AbstractPage {

    public ResultPaymentPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement totalAmountHeader;

    @AndroidFindBy(xpath="")
    private MobileElement pcId;

    @AndroidFindBy(xpath="")
    private MobileElement fullPaySum;

    @AndroidFindBy(xpath="")
    private MobileElement kvitValue;

    @AndroidFindBy(xpath="")
    private MobileElement comissionValue;

    @AndroidFindBy(xpath="")
    private MobileElement totalAmount;

    @AndroidFindBy(xpath="")
    private MobileElement savecard;

    @AndroidFindBy(xpath="")
    private MobileElement getStatus;

    @AndroidFindBy(xpath="")
    private MobileElement goToFinesListButton;

    @AndroidFindBy(xpath="")
    private MobileElement goUSearchButton;

    public String getTotalAmountHeaderValue(){
        return waitAndGetText(totalAmountHeader);
    }

    public String getFullPaymentValue(){
        return waitAndGetText(fullPaySum);
    }

    public String getPcId(){
        return waitAndGetText(pcId);
    }

    public String getKvitValue(){
        return waitAndGetText(kvitValue);
    }

    public String getCommissionValue(){
        return waitAndGetText(comissionValue);
    }

    public String getTotalAmount(){
        return waitAndGetText(totalAmount);
    }

    private void clickSaveCard(){
        waitAndClick(savecard);
    }

    public void clickByGoToFinesListButton(){
        waitAndClick(goToFinesListButton);
    }

    public void clickByGoUSearchButton(){
        waitAndClick(goUSearchButton);
    }

    @Override
    public String waitAndGetText(MobileElement element){
        waitForComplate();
        new WebDriverWait(getDriver(),10).withMessage(element.getAttribute("text")+" element not displayed").until(driver1 -> element.isDisplayed());
        return element.getText();
    }

    private void waitForComplate() {
        new WebDriverWait(getDriver(), 30)
                .pollingEvery(5, TimeUnit.SECONDS)
                .until(driver -> {
                    try {
                        if (pcId.isDisplayed()) {
                            return true;
                        }
                    } catch (WebDriverException e) {
                        try {
                            getStatus.click();
                        } catch (Exception ignored) {
                        }
                    }
                    return false;
                });
    }
}
