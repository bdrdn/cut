package Pages.unversalPages;

import Pages.AbstractPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class AbstractMenu extends AbstractPage {

    private AppiumDriver<MobileElement> driver;

    public AbstractMenu(AppiumDriver<MobileElement> driver){
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement settings;

    @AndroidFindBy(xpath="//*[@resource-id='submit#history']")
    private MobileElement history;

    @AndroidFindBy(xpath = "//*[@resource-id = 'submit#support']")
    private MobileElement support;

    @AndroidFindBy(xpath = "//*[@resource-id = 'submit#askForRating']")
    private MobileElement askForRating;

    @AndroidFindBy(xpath = "//*[@resource-id = 'submit#visaContest']")
    private MobileElement visaBanner;

    @AndroidFindBy(xpath = "//*[@resource-id = 'rating_1']")
    private MobileElement raiting1Button;

    @AndroidFindBy(xpath = "//*[@resource-id = 'rating_5']")
    private MobileElement raiting5Button;

    @AndroidFindBy(xpath = "//*[@text = 'НЕ СЕЙЧАС ']")
    private MobileElement notNowButton;

    @AndroidFindBy(xpath = "//*[@text = 'ОЦЕНИТЬ ']")
    private MobileElement raitingButton;

    @AndroidFindBy(xpath = "//*[@resource-id = 'submit#menu']")
    private MobileElement menuButton;

    public void historyClick() {
        waitAndClick(history);
    }

    protected void settingClick() {
        waitAndClick(settings);
    }

    public void askForRatingClick() {
        waitAndClick(askForRating);
    }

    public void supportClick() {
        waitAndClick(support);
    }



    public BannerPage visaBannerClick() {
        waitAndClick(visaBanner);
        return new BannerPage(driver);
    }

    public RaitingPage raiting1Click() {
        clickMenuButton();
        waitAndClick(askForRating);
        //sleep_for(250);
        waitAndClick(raiting1Button);
        return new RaitingPage(driver);
    }

    public void raiting5Click() {
        clickMenuButton();
        waitAndClick(askForRating);
        //sleep_for(250);
        waitAndClick(raiting5Button);
    }
    private void clickMenuButton(){
        waitAndClick(menuButton);
    }

}
