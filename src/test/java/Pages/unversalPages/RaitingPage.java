package Pages.unversalPages;

import Pages.AbstractPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class RaitingPage extends AbstractPage {

    public RaitingPage(AppiumDriver<MobileElement> driver){
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement textField;

    @AndroidFindBy(xpath="")
    private MobileElement sendResponseButton;

    public void clickBySendResponseButton(String message){
        waitAndFill(textField ,message);
        waitAndClick(sendResponseButton);
    }

}
