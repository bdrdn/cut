package Pages.unversalPages;

import Pages.AbstractPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BannerPage extends AbstractPage {

    private final static Logger logger = LoggerFactory.getLogger(BannerPage.class);
    private AppiumDriver<MobileElement> driver;

    public BannerPage (AppiumDriver<MobileElement> driver){
        super(driver);
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement bannerText;

    @AndroidFindBy(xpath="")
    private MobileElement returnButton;

    public BannerPage validateText(String Text){
        Assert.assertTrue("page text doesn't contain "+Text
                ,waitAndGetText(bannerText).toLowerCase().contains(Text.toLowerCase()));
        return this;
    }

    public void clickReturnButton(){
        waitAndClick(returnButton);
    }

}
