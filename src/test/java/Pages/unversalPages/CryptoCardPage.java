package Pages.unversalPages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import Pages.AbstractPage;

import java.util.List;

import static reps.Utils.sleep_for;

public class CryptoCardPage extends AbstractPage {

    public CryptoCardPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "")
    private MobileElement CVV;
    @AndroidFindBy(xpath = "")
    private MobileElement month;
    @AndroidFindBy(xpath = "")
    private MobileElement year;

    @AndroidFindBy(xpath = "")
    private MobileElement payButton;

    public CryptoCardPage fillCardPayPage(){
        waitAndFill(CVV,"118");
        setMonth();
        setYear();
        return this;
    }

    public void payButtonClick(){
        waitAndClick(payButton);
    }

    private void setMonth(){
        month.click();
        clickListElement("android.widget.RadioButton");
        sleep_for(500);
    }

    private void setYear(){
        year.click();
        clickListElement("android.widget.RadioButton");
        sleep_for(500);
    }
    private void clickListElement(String elementClassName) {
        waitListElement(elementClassName);
        List<MobileElement> list = getDriver().findElementsByClassName(elementClassName);
        list.get(2).click();
    }

    private void waitListElement(String elementClassName) {
        new WebDriverWait(getDriver(), 15)
                .withMessage("need more time for oferta").until(driver -> {
            List<MobileElement> fineItem = getDriver().findElementsByClassName(elementClassName);
            if(fineItem.isEmpty()) return false;
            return fineItem.get(0).isDisplayed();
        });
    }
}
