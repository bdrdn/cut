package Pages.unversalPages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import Pages.AbstractPage;

import static reps.Utils.sleep_for;


public class PayPage extends AbstractPage {
    // private AppiumDriver<MobileElement> driver;

    public PayPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        //this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "")
    private MobileElement sum;

    @AndroidFindBy(xpath = "")
    private MobileElement fioInputField;

    @AndroidFindBy(xpath = "")
    private MobileElement phoneInputField;

    @AndroidFindBy(xpath = "")
    private MobileElement kvit;

    @AndroidFindBy(xpath = "")
    private MobileElement totalCommission;

    @AndroidFindBy(xpath = "")
    private MobileElement totalAmount;

    @AndroidFindBy(xpath = "")
    private MobileElement payButton;

    @AndroidFindBy(xpath = "")
    private MobileElement emailField;

    public String getSumVal() {
        return waitAndGetText(sum);
    }

    public void fillFio(String fioValue) {
        waitAndFill(fioInputField, fioValue);
    }

    public void fillPhone(String phoneValue) {
        waitAndFill(phoneInputField, phoneValue);
    }

    public void kvitClickAndFill(String emailValue) {
        waitAndClick(kvit);
        sleep_for(250);
        waitAndFill(emailField, emailValue);
    }

    public String getTotalCommission() {
        return waitAndGetText(totalCommission);
    }

    public String getTotalAmount() {
        return waitAndGetText(totalAmount);
    }

    public CryptoCardPage payButtonClick() {
        waitAndClick(payButton);
        return new CryptoCardPage(getDriver());
    }
}