package Pages.unversalPages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import Pages.AbstractPage;

public class OfertaPage extends AbstractPage {
    //private AppiumDriver<MobileElement> driver;

    public OfertaPage(AppiumDriver<MobileElement> driver) {
        super(driver);
        //this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath = "")
    private MobileElement ofertaOK;


    @AndroidFindBy(xpath = "")
    private MobileElement ofertaReject;


    public void asseptOferta() {
        waitAndClick(ofertaOK);
    }

    public void rejectOferta() {
        waitAndClick(ofertaReject);
    }
}
