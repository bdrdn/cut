package Pages.SameApp;

import Pages.unversalPages.AbstractMenu;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import Pages.AbstractPage;

public class SettingsPageApp extends AbstractPage {


    public SettingsPageApp(AppiumDriver<MobileElement> driver){
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement iValue;

    @AndroidFindBy(xpath="")
    private MobileElement saveButton;

    @AndroidFindBy(xpath="")
    private MobileElement pushCheckBox;

    @AndroidFindBy(xpath="")
    private MobileElement menuButton;

    public void setIValue(String iValue){
        waitAndFill(this.iValue, iValue);
    }

    public void clickSaveButton(){
        waitAndClick(saveButton);
    }

    public void switchPushCheckBox(){
        waitAndClick(pushCheckBox);
    }

    public AbstractMenu clickMenuButton(){
        waitAndClick(menuButton);
        return new AbstractMenu(getDriver());
    }
}
