package Pages.SameApp;

import Pages.unversalPages.AbstractMenu;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class MenuPageApp extends AbstractMenu {

    public MenuPageApp(AppiumDriver<MobileElement> driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(xpath="")
    private MobileElement n;

    public void clickSearchN() {
        waitAndClick(n);
    }

    public SettingsPageApp settingsClick() {
        super.settingClick();
        return new SettingsPageApp(getDriver());
    }
}
