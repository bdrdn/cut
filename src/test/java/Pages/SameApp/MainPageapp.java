package Pages.SameApp;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import Pages.AbstractPage;

public class MainPageapp extends AbstractPage {

    private static final Logger logger = LoggerFactory.getLogger(MainPageapp.class);

    public MainPageapp(AppiumDriver<MobileElement> driver) {
      super(driver);
        //  this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(xpath="")
    private MobileElement uField;

    @AndroidFindBy(xpath="")
    private MobileElement goToUSearchButton;

    @AndroidFindBy(xpath="")
    private MobileElement menuButton;

    @AndroidFindBy(xpath="")
    private MobileElement iField;

    @AndroidFindBy(xpath="")
    private MobileElement searchButton;

    public void setUValue(String u){
        waitAndFill(uField, u);
    }

    public void setnValue(String iValue) {
        waitAndFill(iField, iValue);
    }

    public MenuPageApp clickByMenuButton() {
        waitAndClick(menuButton);
        return new MenuPageApp(getDriver());
    }

    public void clickBySearchButton() {
        waitAndClick(searchButton);
    }

    public MainPageapp clickByGoToUSearchButton() {
        waitAndClick(goToUSearchButton);
        return this;
    }
}
