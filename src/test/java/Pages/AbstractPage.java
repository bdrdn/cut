package Pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static reps.Utils.sleep_for;

public class AbstractPage {

    private AppiumDriver<MobileElement> driver;

    protected AbstractPage(AppiumDriver<MobileElement> driver){
        this.driver=driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    protected String waitAndGetText(MobileElement element){
        new WebDriverWait(driver,10).withMessage(element.getAttribute("text")+" element not displayed").until(driver1 -> element.isDisplayed());
        return element.getText();
    }

    protected void waitAndClick(MobileElement element){
        int counter = 0;
        try{
            new WebDriverWait(driver,10).withMessage(element.getAttribute("text")+" element not displayed").until(driver1 -> element.isDisplayed());
        } catch (NoSuchElementException e){
            waitAndClick(element,counter);
            return;
        }
        element.click();
    }

    protected void waitAndFill(MobileElement element, String value){
        int counter = 0;
        try {
            new WebDriverWait(driver,10).withMessage(element.getAttribute("text")+" element not displayed").until(driver1 -> element.isDisplayed());
        } catch (StaleElementReferenceException e){
            waitAndFill(element, value,counter);
            return;
        }
        element.clear();
        element.sendKeys(value);
        keyboardHide();
    }

    private void keyboardHide() {
        try{
            driver.hideKeyboard();
        } catch (WebDriverException ignored){}
    }

    private void waitAndFill(MobileElement element, String value, int counter) {
        sleep_for(2500);
        if (counter < 3) {
            counter++;
            try {
                new WebDriverWait(driver, 3).withMessage(element.getAttribute("text") + " element not displayed").until(driver1 -> element.isDisplayed());
            } catch (StaleElementReferenceException e) {
                waitAndFill(element, value, counter);
                return;
            }
            element.clear();
            element.sendKeys(value);
            keyboardHide();
        }
    }

    private void waitAndClick(MobileElement element, int counter) {
        sleep_for(2500);
        if (counter < 3) {
            counter++;
            try {
                new WebDriverWait(driver, 3).withMessage(element.getAttribute("text") + " element not displayed").until(driver1 -> element.isDisplayed());
            } catch (NoSuchElementException e) {
                waitAndClick(element, counter);
                return;
            }
            element.click();
        }
    }


    protected AppiumDriver<MobileElement> getDriver() {return driver;}
}
