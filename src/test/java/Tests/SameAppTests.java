package Tests;

import inter.MobileLogic;
import inter.MobilePaymentLogic;
import Enums.PaymentType;
import Logic.SameAppLogic;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.junit.*;
import reps.AppUtils;
import reps.AppiumWorker;
import reps.Rules.LogRule;
import reps.Rules.ResultCollector;
import reps.a.CaseID;

import static Enums.Documents.APP_DOC_1;
import static Enums.Documents.APP_DOC;

public class SameAppTests {

    private AppiumDriver<MobileElement> driver;
    private MobilePaymentLogic paymentLogic;
    private MobileLogic logic;

    @Rule
    public LogRule logRule = new LogRule();

    @Rule
    public ResultCollector rc = new ResultCollector();


    @BeforeClass
    public static void upAp() {
        new AppiumWorker().startServer();
    }

    @AfterClass
    public static void closeAp() {
        new AppiumWorker().stopServer();
    }

    @Before
    public void SetUp() {
        this.driver = new AppUtils().setup("sameType");
        SameAppLogic l = new SameAppLogic(driver);
        this.logic = l;
        this.paymentLogic = l;
    }

    @CaseID({"18283","18284","18292"})
    @Test
    public void C18292(){
        paymentLogic
                .<SameAppLogic>asseptOferta()
                .search(APP_DOC_1)
                .validateData()
                .pay(true,PaymentType.NOT3DS)
                .assertPayment();
        rc.addComment(paymentLogic.getPaymentId());
    }

    @CaseID({"18288","18292","18285"})
    @Test
    public void C18288(){
        paymentLogic
                .<SameAppLogic>asseptOferta()
                .search(APP_DOC)
                .validateData()
                .pay(false,PaymentType.NORMAL)
                .assertPayment();
        rc.addComment(paymentLogic.getPaymentId());
    }

    @CaseID("18287")
    @Test
    public void C18287(){
        paymentLogic
                .<SameAppLogic>asseptOferta()
                .search(APP_DOC_1)
                .validateData()
                .pay(false,PaymentType.CANCEL);
    }

    @Test
    public void save() {
        paymentLogic.<SameAppLogic>asseptOferta();
        logic.saveCharges(APP_DOC_1).checkSaveCharges();
    }

}