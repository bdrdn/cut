
import Tests.SameAppTests;
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.slf4j.MDC;
import reps.Rules.getLogToken;
import reps.TestRail.rail;
import reps.lilRunnerHelp.TestRunnerListener;
import reps.lilRunnerHelp.Testfilter;

import java.util.Arrays;
import java.util.List;

import static reps.Params.testrail_parent_run_id;

public class TestRunner {

    {
        String logToken = getLogToken.getLogToken(Thread.currentThread());
        MDC.put("logToken", logToken);
    }

    public static void main(String[] args) {
        TestRunner testRunner = new TestRunner();
        testRunner.run();
    }

    private Class[] TestClass = {
            SameAppTests.class
    };

    private void run() {
        reps.Params.run_id = new rail().clone_run(testrail_parent_run_id);
        String CaseIDs = new rail().parseCaseIdToString(reps.Params.run_id);
        String tagsParam = System.getProperty("case_id", CaseIDs);
        List<String> CaseIDList = Arrays.asList(tagsParam.split(",\\s"));
        Request request = Request.classes(TestClass);
        Testfilter testfilter = new Testfilter(CaseIDList);
        request = request.filterWith(testfilter);
        JUnitCore jUnitCore = new JUnitCore();
        jUnitCore.addListener(new TestRunnerListener());
        jUnitCore.run(request);
        MDC.clear();
    }
}
